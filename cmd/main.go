package main

import (
	"fmt"
	"github.com/gusleein/goconfig"
	"github.com/gusleein/golog"
	"go-telegram-bot"
	t "go-telegram-bot/localizator"
	"os"
	"os/signal"
	"runtime"
	"syscall"
)

var (
	token, env, lang string
)

func main() {
	telegramBot.Run(token)

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	<-c
}

func init() {
	env, token, lang = args()
	config.Init(env)

	log.Init(true, "console")
	log.Info("Logs ok")
	log.Info("GOMAXPROCS: ", runtime.GOMAXPROCS(0))

	t.Init(lang)
}

func args() (env, token, lang string) {
	if len(os.Args) == 1 && (os.Args[1] == "help" || os.Args[1] == "--help") {
		fmt.Print(` 
Usage:
	server [env] [token]

Available environments:                                                            
	dev
	prod

token - telegram bot token
	
`)
		os.Exit(0)
	}
	if len(os.Args) < 3 {
		panic("You need to enter three arguments. \nThe first argument is 'Config'. \nThe Third argument is 'ServiceName'")
	}

	env = os.Args[1]
	token = os.Args[2]
	lang = os.Args[3]
	return
}
