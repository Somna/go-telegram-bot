package telegramBot

import (
	tb "gopkg.in/tucnak/telebot.v2"

	"go-telegram-bot/localizator"
)

func MakeInlineKeyboard(keyboard [][]tb.InlineButton) *tb.ReplyMarkup {
	return &tb.ReplyMarkup{
		InlineKeyboard: keyboard,
	}
}

func MakeBackButton(data, unique, lang string) []tb.InlineButton {
	return []tb.InlineButton{
		{
			Data:   data,
			Unique: unique,
			Text:   "↩️ " + t.Get("common", "button_back", lang),
		},
	}

}

func MakeBackDevsButtonRow(lang, network string) []tb.InlineButton {
	return MakeBackButton(network, "back_devs", lang)
}
