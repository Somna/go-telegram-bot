package handlers

import (
	log "github.com/gusleein/golog"
	t "go-telegram-bot/localizator"
	tb "gopkg.in/tucnak/telebot.v2"
)

func c(bot *tb.Bot, handler func(b *tb.Bot, cb *tb.Callback)) func(cb *tb.Callback) {
	return func(cb *tb.Callback) {
		handler(bot, cb)
	}
}

func setCallback(b *tb.Bot, unique string, handler func(m *tb.Callback)) {
	b.Handle(&tb.InlineButton{Unique: unique}, metricsCallbackMiddleware(unique, func(m *tb.Callback) {
		m.Message.Sender = m.Sender
		handler(m)
		sendRespond(b, m)
	}))
}

func setHandler(b *tb.Bot, unique string, handler func(m *tb.Message)) {
	b.Handle(unique, metricsHandlerMiddleware(unique, handler))
}

func sendResponse(b *tb.Bot, chat tb.Recipient, content interface{}, options ...interface{}) (msg *tb.Message, err error) {
	options = append(options, tb.Silent, tb.ModeHTML)
	msg, err = b.Send(chat, content, options...)
	if err != nil {
		log.Error(err.Error())
	}
	//metrics.TelegramSendMessage.WithLabelValues("response", errtext).Inc()

	return
}

func sendRespond(b *tb.Bot, c *tb.Callback) {
	err := b.Respond(c, &tb.CallbackResponse{})
	if err != nil {
		log.Error(err.Error())
	}
}

func metricsHandlerMiddleware(path string, f func(m *tb.Message)) func(m *tb.Message) {
	return func(m *tb.Message) {
		//metrics.TelegramReceiveMessage.WithLabelValues("handler", path).Inc()
		f(m)
	}
}

func metricsCallbackMiddleware(path string, f func(m *tb.Callback)) func(m *tb.Callback) {
	return func(m *tb.Callback) {
		//metrics.TelegramReceiveMessage.WithLabelValues("callback", path).Inc()
		f(m)
	}
}

func sendInternalError(b *tb.Bot, m *tb.Message) {
	log.Debugw("send internal error message", "chat", m.Chat.ID)
	b.Send(m.Chat, t.Get("common", "response_internal_err", m.Sender.LanguageCode), tb.Silent)
}
