package handlers

import (
	log "github.com/gusleein/golog"
	"go-telegram-bot/localizator"
	"go-telegram-bot/tg"
	tb "gopkg.in/tucnak/telebot.v2"
)

func showMenu(locale string) tb.ReplyMarkup {
	log.Debug(locale)
	// todo get selfies from db
	//selfies := db.Selfies.Get()
	selfies := 0
	builder := &tg.MenuBuilder{}
	builder.Add(0, tg.InlineButton(t.Get("admin", "event_settings", locale), "event settings"))
	builder.Add(1, tg.InlineButton(t.Get("admin", "show_admins", locale), "show admins"))
	builder.Add(2, tg.InlineButton(t.Get("admin", "ban_users", locale), "show ban users"))
	builder.Add(2, tg.InlineButton(t.Get("admin", "permanent_whitelist", locale), "show permanent white list"))
	builder.Add(3, tg.InlineButton(t.Get("admin", "check_user_tickets", locale), "get user tickets"))
	builder.Add(4, tg.InlineButton(t.Get("admin", "new_mailing", locale), "create new mailing"))
	builder.Add(5, tg.InlineButton(t.Get("admin", "qiwi_tokens", locale), "show qiwi tokens"))
	builder.Add(6, tg.InlineButton(t.Get("admin", "selfies_on_check", locale, selfies), "get selfie requests"))

	return builder.Build()
}

func SendMenu(bot *tb.Bot) func(cb *tb.Message) {
	return func(cb *tb.Message) {
		log.Debug("admin menu")
		keyboard := showMenu(cb.Sender.LanguageCode)
		if _, err := sendResponse(bot, cb.Chat, t.Get("admin", "admin_menu_title", cb.Sender.LanguageCode), &keyboard); err != nil {
			log.Errorw("err send admin menu", "err", err)
		}
	}
}
