package handlers

import (
	log "github.com/gusleein/golog"
	tb "gopkg.in/tucnak/telebot.v2"
	"time"
)

func Init(token string) {
	bot, err := tb.NewBot(tb.Settings{
		Reporter: func(e error) {
			if e != nil {
				log.Error(e)
			}
		},
		Token:  token,
		Poller: &tb.LongPoller{Timeout: 5 * time.Second},
	})
	if err != nil {
		log.Fatal(err)
	}

	// init bot
	setHandler(bot, "/start", MakeHandlerHelp(bot))
	setHandler(bot, "/help", MakeHandlerHelp(bot))

	setCallback(bot, "empty", func(m *tb.Callback) {})

	bot.Handle(tb.OnText, func(m *tb.Message) {
		//v, ok := cacheStates.Get(fmt.Sprint(m.Chat.ID))
		//if !ok {
		//	return
		//}
		//
		////state := v.(cmdState)
		//
		//
		//cacheStates.Delete(fmt.Sprint(m.Chat.ID))
	})

	// init admin commands
	setHandler(bot, "/admin", SendMenu(bot))
	setHandler(bot, "/a", SendMenu(bot))
	setHandler(bot, "/а", SendMenu(bot))
	setHandler(bot, "/админ", SendMenu(bot))
	setHandler(bot, "/адмін", SendMenu(bot))

	go bot.Start()

	log.Info("Started: telegram bot")
}

func initAdmin(bot *tb.Bot) {
}
