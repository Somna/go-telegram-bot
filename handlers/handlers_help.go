package handlers

import (
	t "go-telegram-bot/localizator"
	tb "gopkg.in/tucnak/telebot.v2"
)

func MakeHandlerHelp(b *tb.Bot) func(m *tb.Message) {
	return func(m *tb.Message) {
		sendHelp(b, m)
	}
}

func MakeCallbackHelp(b *tb.Bot) func(m *tb.Callback) {
	return func(m *tb.Callback) {
		sendHelp(b, m.Message)
	}
}

func sendHelp(b *tb.Bot, m *tb.Message) {
	sendResponse(b, m.Chat, t.Get("common", "screen_help", m.Sender.LanguageCode))
}
