package tg

import (
	tb "gopkg.in/tucnak/telebot.v2"
)

type MenuBuilder struct {
	buttons [][]tb.InlineButton
}

func (mb MenuBuilder) GroupSpace() {

}

func (mb *MenuBuilder) Add(row int, btn tb.InlineButton) {
	// если пустой, то init
	if len(mb.buttons) == 0 {
		mb.buttons = make([][]tb.InlineButton, 1)
	}
	// если указанный row превышает кол-во текущих row, то нужно добавить
	// сколько не хватает пустыми ячейками
	if len(mb.buttons) <= row {
		// начинаем с последнего элемента и добавляем новые, пока не достигнем
		// указанный row
		for i := len(mb.buttons) - 1; i <= row; i++ {
			mb.buttons = append(mb.buttons, make([]tb.InlineButton, 0))
		}
	}
	mb.buttons[row] = append(mb.buttons[row], btn)
}

func (mb *MenuBuilder) Build() tb.ReplyMarkup {
	return tb.ReplyMarkup{InlineKeyboard: mb.buttons}
}

func InlineButton(text, callbackData string) tb.InlineButton {
	return tb.InlineButton{
		Text: text,
		Data: callbackData,
	}
}

func InlineButtonUrl(text, url, callbackData string) tb.InlineButton {
	return tb.InlineButton{
		Text: text,
		URL:  url,
		Data: callbackData,
	}
}
