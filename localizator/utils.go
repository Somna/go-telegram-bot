package t

import "strings"

func parseLocale(filename string) string {
	if len(filename) == 0 {
		return ""
	}
	filenameArr := strings.Split(filename, ".")
	// если кроме расширения и языка есть еще часть в имени файла
	// парсим такой формат (a.b.c.en.json)
	if len(filenameArr) > 2 {
		return filenameArr[len(filenameArr)-2 : len(filenameArr)][0]
	}
	// иначе возвращаем 1 элемент
	// парсим такой формат (en.json)
	return filenameArr[0]
}
