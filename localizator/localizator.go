package t

import (
	"encoding/json"
	"fmt"
	log "github.com/gusleein/golog"
	"os"
	"path/filepath"
	"sync"

	"golang.org/x/text/language"
	"golang.org/x/text/message"
)

var (
	locales       sync.Map
	defaultLocale string
)

// Init - задает
func Init(lang string) {
	_, err := language.Parse(lang)
	if err != nil {
		log.Fatal(err)
	}
	defaultLocale = lang
}

// Parse - выгружает локали из указанной дир-ии (path) в store в соотв space
func Parse(path, space string) {
	files, err := os.ReadDir(path)
	if err != nil {
		log.Fatal(err)
	}

	// перебираем файлы локализации из указанной дир-ии path
	for _, file := range files {
		filename := file.Name()
		// localeTag - идентификатор языка в формате BCP 47 ("en" "ru" "smj")
		localeTag, err := language.Parse(parseLocale(filename))
		if err != nil {
			log.Fatal(fmt.Sprintf("Parsing files. Filename %s, error: %v", filename, err))
		}

		b, err := os.ReadFile(filepath.Join(path, filename))
		if err != nil {
			err = fmt.Errorf("Reading file. Filename %s, error: %v", filename, err)
			log.Fatal(err)
		}

		// парсим json в map'у
		localeMap := make(map[string]interface{})
		if err = json.Unmarshal(b, &localeMap); err != nil {
			err = fmt.Errorf("Serialization file. Filename %s, error: %v", filename, err)
			log.Fatal(err)
		}

		// сохраняем переводы в глобальный Store
		locales.Store(localeTag.String()+"."+space, localeMap)

		// используем стандартную либу для хранения каталога сообщений
		for key, v := range localeMap {
			if space != "" {
				key = space + "." + key
			}
			if msg, ok := v.(string); ok {
				// фиксируем в каталог сообщений
				message.SetString(localeTag, key, msg)
			}
		}
	}
}

// Get - отдает сообщение по space и locale
func Get(space, key, locale string, tags ...interface{}) string {
	return get(locale, key, space, tags...)
}

// get - keys принимает либо один аргумент key, либо два key и space
func get(locale, key, space string, tags ...interface{}) string {
	if key == "" {
		return ""
	}
	if space != "" {
		key = space + "." + key
	}
	tag := message.MatchLanguage(locale, "en")
	return message.NewPrinter(tag).Sprintf(message.Key(key, ""), tags...)
}
