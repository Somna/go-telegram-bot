package t

import (
	log "github.com/gusleein/golog"
	"testing"
)

func TestGetLocale(t *testing.T) {
	log.Init(true, log.Console)

	Parse("../locales", "common")
	value := Get("common", "button_back", "en")
	if value != "" && value != "BACK" {
		t.Error("invalid", value)
	}
	log.Debug(value)

	Parse("../locales", "admin")
	value = Get("common", "Create new mailing", "en")
	if value != "" && value != "Create new mailing" {
		t.Error("invalid", value)
	}
	log.Debug(value)
}
