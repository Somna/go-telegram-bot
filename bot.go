package telegramBot

import (
	"github.com/gusleein/golog"
	"go-telegram-bot/handlers"
	"go-telegram-bot/localizator"
)

func Run(token string) {
	log.Info("Run: telegram bot...")
	// Загружаем локализации в память
	t.Parse("./locales", "common")
	t.Parse("./locales", "admin")
	//localizator.Parse("./locales", "errors")
	//localizator.Parse("./locales", "user")
	handlers.Init(token)
}
